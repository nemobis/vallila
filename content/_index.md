---
title: "Vallila"
date: 2022-11-26T22:04:29+02:00
---

## Vallila (Wikipediasta)

Vallila (ruots. Vallgård) on pohjoisimpia Helsingin kantakaupunkiin kuuluvia kaupunginosia. 

### Kuva

![Vallila, Suvannontie, 2005-07](/Vallila,_Suvannontie,_July_2005.jpg)

[Vallila, Helsinki, Finland](https://commons.wikimedia.org/wiki/File:Vallila,_Suvannontie,_July_2005.jpg), Hyperboreios, 2005, Public Domain.

### Kuvaus

Vallilan pohjoispuolelle jää Kumpula, eteläpuolelle jää Alppiharju, itäpuolelle jää Hermanni ja länsipuolelle jää Pasila. Rajana Hermannia vastaan on Hämeentie, Alppiharjua vastaan Aleksis Kiven katu.

Vallilan ja Kumpulan rajalla on leveä viheralue, jossa aivan Vallilan kerrostaloalueen vieressä on myös siirtolapuutarha. Vallilan kautta kulkevia vilkasliikenteisiä liikenneväyliä ovat myös Mäkelänkatu, Sturenkatu ja Teollisuuskatu. Vallilassa on asukkaita 10 375 (2019) ja työpaikkoja 20 300 (31.12.2015).

Vallilan näkyvimpiin rakennuksiin kuuluu Paavalinkirkko, joka sijaitsee viheralueen reunassa lähellä Hämeentien ja Sturenkadun risteystä.

### Historia

Vallila kuului alun perin Helsingin asemakaavoitetun alueen liepeille 1840-luvulla muodostettuihin vuokra-alueisiin. Ruotsinkielinen nimi Vallgård juontuu siitä, että alue oli aikoinaan Helsingin kaupunkilaisten karjalaitumena (ruots. vall 'nurmi'). Suomenkielinen nimi Vallila virallistettiin vuonna 1909 ja vahvistettiin kaupunginosan nimeksi vuonna 1959. Alueelle laadittiin jaotussuunnitelma vuonna 1908 ja lopullinen asemakaava vahvistettiin vuonna 1940.

Vallila on suurelta osin kerrostaloaluetta, mutta siellä on myös 1910-luvulla rakennettu pientaloalue, Puu-Vallila. Se oli pitkään purku-uhan alaisena, mutta se kunnostettiin 1980-luvulla.

Vallilan eteläosa vanhan Sörnäisten satamaradan molemmin puolin rakennettiin 1900-luvun alussa teollisuusalueeksi. Suurin siellä sijainnut teollisuuslaitos oli VR:n Pasilan konepaja, joka siis nimestään huolimatta sijaitsi Vallilan puolella. Satamarata siirrettiin 1960-luvulla kulkemaan Vallilan pohjoispuolitse, ja sen jälkeen vanhan satamaradan varsilla olleet teollisuusrakennukset on vähitellen purettu tai muutettu toimistorakennuksiksi. Entiselle VR:n konepajan alueelle rakennettiin 2010-luvulla uusi asuntoalue, Konepaja. Volvo-autoja maahantuoneen Oy Volvo-Auto Ab:n päätoimipaikka sijaitsi 1980-luvun lopulle saakka yhtiön rakennuttamassa, juuri ennen talvisotaa valmistuneessa kiinteistössä Sturenkadun varrella. Kyseinen rakennus on luokiteltu rakennushistoriallisesti arvokkaaksi ja on suojeltu.

Vallilan Elimäenkadulla sijaitsi Sonera-teleyhtiön (1998–2002) pääkonttori.

Mäkelänrinteen uintikeskuksen ja Mäkelänrinteen lukion väliin valmistui vuonna 2021 Urhea-halli ja sen läheisyyteen Urhea-koti. Ne muodostavat yhdessä Urhea-kampuksen, joka toimii Suomen huippu-urheilun keskuksena. MTV muuttaa vuoden 2022 joulukuussa Konepajan alueelle NCC:ltä vuokrattaviin tiloihin Fredriksbergin D-talossa.

## Tekijät

* Wikipedia-projektin osanottajat, [*Vallila*](https://fi.wikipedia.org/w/index.php?title=Vallila&oldid=21010739), Wikipedia (haettu 2022-11-26).
